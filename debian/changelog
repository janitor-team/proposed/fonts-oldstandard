fonts-oldstandard (2.2really-4) unstable; urgency=medium

  * Team upload.
  * debian/control:
    + Remove Christian Perrier from uploaders list. (Closes: #927621)
      Thank you for your previous work!
    + Bump debhelper compat to v13.
    + Bump Standards-Version to 4.5.0.
    + Update Vcs-* fields to use git packaging repo under Salsa
      fonts-team.
  * debian/gbp.conf: Removed in favour of default parameters.
  * debian/rules: Use latest grammar and remove nonstandard arguments.
  * debian/patches: Add patch to make ost-generate.py compatible with
    python3. (Closes: #950100)

 -- Boyuan Yang <byang@debian.org>  Mon, 25 May 2020 20:27:06 -0400

fonts-oldstandard (2.2really-3) unstable; urgency=low

  * Point Homepage to http://www.thessalonica.org.ru/en/oldstandard.html
    Closes: #600324
  * Update Standards to 3.9.5 (checked)
  * Bump debhelper compatibility to 9
  * Drop transitional package
  * Add 'Multi-Arch: foreign' field
  * Use 'Breaks' instead of 'Conflicts'. Drop 'Provides' as it is no
    longer needed (installations should have transitioned since wheezy
    and the package has anyway no reverse dependency.
  * Use xz extreme compression for deb packages
  * Use git for packaging: adapt Vcs-* fields

 -- Christian Perrier <bubulle@debian.org>  Sun, 05 Jan 2014 15:55:28 +0100

fonts-oldstandard (2.2really-2) unstable; urgency=low

  * Add an extended description to the dummy ttf-oldstandard package

 -- Christian Perrier <bubulle@debian.org>  Mon, 13 Jun 2011 22:14:41 +0200

fonts-oldstandard (2.2really-1) unstable; urgency=low

  * New upstream release (real 2.2 as the former Debian packages has
    2.02). Closes: #628394
  * Build-Depend on a versioned fontforge as building with FF 20100501
    fails.
  * Bump Standards to 3.9.2 (checked, no change)
  * Rename source package to "fonts-oldstandard" to fit the Font
    Packages Naming Policy. No mention of foundry seems to be needed
    here.

 -- Christian Perrier <bubulle@debian.org>  Mon, 13 Jun 2011 16:41:52 +0200

ttf-oldstandard (2.2-3) unstable; urgency=low

  * Use "fontforge-nox | fontforge" in Build-Depends. Drop
    versioned build dependency on pre-lenny version of fontforge
  * Update Standards to 3.9.1 (checked, no change)

 -- Christian Perrier <bubulle@debian.org>  Sun, 13 Feb 2011 16:48:45 +0100

ttf-oldstandard (2.2-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Use TTF layer to fix rendering problems. Closes: #600306

 -- Jan Lübbe <jluebbe@debian.org>  Mon, 25 Oct 2010 20:16:45 +0200

ttf-oldstandard (2.2-2) unstable; urgency=low

  * Drop fontforge from Recommends. Closes: #568662
  * Drop fontconfig from Depends.
  * Update Standards to 3.8.4 (checked, no change)

 -- Christian Perrier <bubulle@debian.org>  Sun, 07 Feb 2010 12:39:06 +0100

ttf-oldstandard (2.2-1) unstable; urgency=low

  * New upstream version
  * Switch to debhelper v7
  * Use a minimal debian/rules file
  * Switch to 3.0 (quilt) source format
  * Add ${misc:Depends} to dependencies to properly cope with
    debhelper-triggerred dependencies
  * Add myself as Uploader
  * Update Standards to 3.8.3 (checked)
  * Drop defoma use

 -- Christian Perrier <bubulle@debian.org>  Thu, 10 Dec 2009 07:10:39 +0100

ttf-oldstandard (1.0-2) unstable; urgency=low

  * Fixed Build-Deps
  * Simplified the build process using fontforge's new Python interface
  * Added full font sources including xgridfit source files
  * Some cleanup of the packaging description, name and script
  * Added the defoma hints

 -- Nicolas Spalinger <nicolas.spalinger@sil.org>  Sun,  3 Jun 2007 18:34:39 +0100

ttf-oldstandard (1.0-1) unstable; urgency=low

  * Initial Release. (Closes: #408939)

 -- Pantelis Koukousoulas <pakt223@freemail.gr>  Sat, 17 Mar 2007 21:42:00 +1000
